<?php

namespace App\DataProvider;

use App\Entity\Pokemon;
use App\Repository\PokemonRepository;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\State\ProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class PokemonProvider implements ProviderInterface
{
    private $pokemons;
    private $manager;
    private $tokenStorage ;
    private $authorizationChecker ;

    public function __construct(PokemonRepository $pokemonRepository, EntityManagerInterface $manager, TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->manager = $manager;
        $this->pokemons = $pokemonRepository;
        $this->tokenStorage  = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Provides data. Récupère la collection de pokemon selon les conditions définie avant (isLegendary ou non)
     *
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     *
     * @return T|Pagination\PartialPaginatorInterface<T>|iterable<T>|null
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        if ($operation instanceof CollectionOperationInterface) {

            $allPokemon = $this->pokemons->getAllPokemon();

            return $allPokemon;
        }
    }
}