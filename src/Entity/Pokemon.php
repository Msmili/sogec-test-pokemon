<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\SecurityController;
use App\DataProvider\PokemonProvider;
use App\Repository\PokemonRepository;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Security\Voter\PokemonVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PokemonRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['noLegendary:write']],
    operations : [
        new GetCollection(
            provider: PokemonProvider::class,
        ),
        new Get(),
        new Patch(
            security: "is_granted('POKE_EDIT', object)",
        ),
        new Delete(
            security: "is_granted('POKE_DELETE', object)",
        ),
    ],
    paginationItemsPerPage: 50,
    paginationClientItemsPerPage: true,
)]
#[ApiFilter(
SearchFilter::class, properties: [
    'name' => SearchFilter::STRATEGY_PARTIAL,
    'number' => 'exact',
    'type1'=> 'exact',
    'type2'=> 'exact',
    'generation'=> 'exact',
    'legendary'=> 'exact',
]
)]
class Pokemon
{

    public const all_types = [
        'Normal', 'Fire', 'Water', 'Grass', 'Electric', 'Ice', 'Fighting', 'Poison',
        'Ground', 'Flying', 'Psychic', 'Bug', 'Rock', 'Ghost', 'Dark', 'Dragon', 'Steel', 'Fairy'
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $number = null;

    #[ORM\Column(length: 150)]
    #[Groups("noLegendary:write")]
    private ?string $name = null;

    #[ORM\Column(length: 50)]
    #[Groups("noLegendary:write")]
    private ?string $type1 = null;

    #[ORM\Column(length: 50, nullable: true, options:["default" => ""])]
    #[Groups("noLegendary:write")]
    private ?string $type2 = '';

    #[ORM\Column]
    private ?int $total = null;

    #[ORM\Column]
    private ?int $hp = null;

    #[ORM\Column]
    private ?int $attack = null;

    #[ORM\Column]
    private ?int $defense = null;

    #[ORM\Column]
    private ?int $spAtk = null;

    #[ORM\Column]
    private ?int $spDef = null;

    #[ORM\Column]
    private ?int $speed = null;

    #[ORM\Column]
    #[Groups("noLegendary:write")]
    private ?int $generation = null;

    #[ORM\Column]
    private ?bool $legendary = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): static
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getType1(): ?string
    {
        return $this->type1;
    }

    public function setType1(string $type1): static
    {
        $this->type1 = $type1;

        return $this;
    }

    public function getType2(): ?string
    {
        return $this->type2;
    }

    public function setType2(?string $type2): static
    {
        $this->type2 = $type2;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): static
    {
        $this->total = $total;

        return $this;
    }

    public function getHp(): ?int
    {
        return $this->hp;
    }

    public function setHp(int $hp): static
    {
        $this->hp = $hp;

        return $this;
    }

    public function getAttack(): ?int
    {
        return $this->attack;
    }

    public function setAttack(int $attack): static
    {
        $this->attack = $attack;

        return $this;
    }

    public function getDefense(): ?int
    {
        return $this->defense;
    }

    public function setDefense(int $defense): static
    {
        $this->defense = $defense;

        return $this;
    }

    public function getSpAtk(): ?int
    {
        return $this->spAtk;
    }

    public function setSpAtk(int $spAtk): static
    {
        $this->spAtk = $spAtk;

        return $this;
    }

    public function getSpDef(): ?int
    {
        return $this->spDef;
    }

    public function setSpDef(int $spDef): static
    {
        $this->spDef = $spDef;

        return $this;
    }

    public function getSpeed(): ?int
    {
        return $this->speed;
    }

    public function setSpeed(int $speed): static
    {
        $this->speed = $speed;

        return $this;
    }

    public function getGeneration(): ?int
    {
        return $this->generation;
    }

    public function setGeneration(int $generation): static
    {
        $this->generation = $generation;

        return $this;
    }

    public function isLegendary(): ?bool
    {
        return $this->legendary;
    }

    public function setLegendary(bool $legendary): static
    {
        $this->legendary = $legendary;

        return $this;
    }
}
