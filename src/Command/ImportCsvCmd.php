<?php

    namespace App\Command;

    use App\Entity\Pokemon;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Attribute\AsCommand;


    class ImportCsvCmd extends Command {
        //ici obligé de mettre defaultName pour la commande
        protected static $defaultName = "app:import:csv";
        //Pour gérer l'ordre du csv
        private $header = ['#','Name','Type 1','Type 2','Total','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed','Generation','Legendary'];

        private $entityManager;

        public function __construct(EntityManagerInterface $entityManager) {
            $this->entityManager = $entityManager;
            parent::__construct();
        }

        protected function configure() {
            $this->setDescription('Import CSV file')
                ->addArgument('filepath', InputArgument::REQUIRED, 'Path to CSV file');
        }

        /**
         * Execute la commande
         *
         * @param InputInterface $input
         * @param OutputInterface $output
         * @return int|void
         * @throws \Exception
         */
        protected function execute(InputInterface $input, OutputInterface $output) {

            $filePath = $input->getArgument('filepath');

            //On récupère les donénes du csv
            $datas = $this->getData($filePath);
            //Insertion en BDD des données
            $this->insert($datas);

            $output->writeln('Importing CSV from: ' . $filePath);

            return Command::SUCCESS;
        }

        /**
         * Récupération des données du csv et formatage des données
         * @param $filePath
         * @return array
         * @throws \Exception
         */
        protected function getData($filePath) {

            // Vérifier le type de fichier
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            if ($fileExtension !== 'csv') {
                throw new \Exception('Le fichier doit être au format CSV.');
            }

            $csvData = [];
            if (($handle = fopen($filePath, "r")) !== false) {
                //On récuèpre la première ligne du CSV
                $firstRow = fgetcsv($handle, 1000, ",");

                if ( $firstRow !== $this->header) {
                    $indexCsv = [];

                    //Je parcours les éléments de la première ligne du csv pour remettre dans l'ordre les index
                    foreach($this->header as $model) {
                        $indexCsv[] = array_search($model, $firstRow);
                    }

                    // Je continue à parcourir les éléments du csv pour réograniser les données
                    while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                        // Créer un nouvel ordre de colonnes en fonction des indices
                        $changeIndex = [];
                        foreach ($indexCsv as $index) {
                            $changeIndex[] = is_numeric($data[$index]) ? intval($data[$index]) : $data[$index];
                        }
                        $csvData[] = $changeIndex;
                    }

                } else {

                    while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                        $csvData[] = is_numeric($data) ? intval($data) : $data;
                    }
                }

                fclose($handle);
            }
            return $csvData;

        }

        /**
         * Insertion des données du csv en base de données
         * @param $datas
         */
        protected function insert($datas)
        {
            //Je parcours mes données pour les associé a leur champs
            foreach ($datas as $row) {
                $entity = new Pokemon();
                $entity->setNumber($row[0]);
                $entity->setName($row[1]);
                $entity->setType1(ucfirst($row[2]));
                $entity->setType2(ucfirst($row[3]));
                $entity->setTotal($row[4]);
                $entity->setHp($row[5]);
                $entity->setAttack($row[6]);
                $entity->setDefense($row[7]);
                $entity->setSpAtk($row[8]);
                $entity->setSpDef($row[9]);
                $entity->setSpeed($row[10]);
                $entity->setGeneration($row[11]);
                if ($row[12] == 'false' || $row[12] == 'False' || $row[12] == false ){
                    $entity->setLegendary(0);
                } else {
                    $entity->setLegendary(1);
                }


                //Je commit les données des champs
                $this->entityManager->persist($entity);
            }
            //Je push les données en BDD pour les insérer
            $this->entityManager->flush();
        }

    }

