<?php

namespace App\Controller;

use App\Form\LoginFormType;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Config\SecurityConfig;
use Symfony\Contracts\Service\Attribute\SubscribedService;


class SecurityController extends AbstractController
{

    /**
     * Chemin login pour l'api
     *
     * @param SecurityConfig $securityConfig
     * @return Response
     */
    #[Route('/api/login', name: 'app_security_login', methods: ['POST'])]
    public function login(SecurityConfig $securityConfig): Response
    {
        //Configuration du firewall (security.yaml) car besoin de de ces paramètre pour l'api et conflit avec l'interface utilisateur
        $securityConfig->firewall('main')
            ->pattern('^/api/login')
            ->stateless(true);
        $user = $this->getUser();

        return $this->json([
            'email' => $user->getUsername()
        ]);
    }

    /**
     * Permet la connexion utilisateurs.
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    #[Route('/connexion', name: 'security.connexion', methods: ['GET', 'POST'])]
    public function loginInterface(AuthenticationUtils $authenticationUtils, SecurityConfig $securityConfig): Response
    {
        if(!$this->getUser()) {var_dump($this->getUser());
            return $this->render('/security/login.html.twig', [
                'last_username' => $authenticationUtils->getLastUsername(),
                'error' => $authenticationUtils->getLastAuthenticationError()
            ]);
        } else {
            return $this->redirectToRoute('pokemon.all');
        }

    }

    /**
     * Permet de se déconnecté
     */
    #[Route('/logout', name: 'security.logout', methods: ['GET', 'POST'])]
    public function logout(){
        //Rien a faire ici
    }

    /**
     * Permet de faire l'inscription utilisateurs.
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/inscription', name: 'security.registration', methods : ['GET', 'POST'])]
    public function registration(Request $request, EntityManagerInterface $manager) : Response {
        $user = new User();
        //On définie le role utilisateur par défaut a ROLE_USER
        $user->setRoles(['ROLE_USER']);

        $form = $this->createForm(LoginFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $this->addFlash(
                'success',
                'Votre compte est créée.'
            );

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security.connexion');
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
