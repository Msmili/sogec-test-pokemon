<?php

namespace App\Controller;

use App\Entity\Pokemon;
use App\Entity\User;
use App\Form\PokemonType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\PokemonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PokemonController extends AbstractController
{
    /**
     * Permet d'afficher la collection de pokemon (tout les pokemon en BDD)
     *
     * @param PokemonRepository $pokemonRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/pokemon', name: 'pokemon.all')]
    public function index(PokemonRepository $pokemonRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $queryBuilder = $pokemonRepository->createQueryBuilder('p');

        if (!$this->getUser()) {
            $queryBuilder->where('p.legendary = :isLegendary')
                ->setParameter('isLegendary', false);
        }

        $query = $queryBuilder->getQuery();


        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('pokemon/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet d'editer un pokemon
     *
     * @param Pokemon $pokemon
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[isGranted('ROLE_USER')]
    #[Route('/edit_pokemon/edit/{id}', name:'poke.edit', methods: ['GET','POST'])]
    public function edit(Pokemon $pokemon, Request $request, EntityManagerInterface $manager) : Response{
        // Récupérer l'utilisateur actuellement connecté
        $currentUser = $this->getUser();

        // Vérifier que l'utilisateur possède bien le rôle ROLE_USER
        if (!$this->isGranted('ROLE_USER')) {
            // Redirection vers une page d'accès refusé
            return new RedirectResponse($this->generateUrl('security.connexion'));
        }

        // Vérifier que l'utilisateur est le même que celui associé à la tâche
        if (!$this->getUser()) {
            // Redirection vers une page d'accès refusé
            return new RedirectResponse($this->generateUrl('pokemon.all'));
        }

        $form = $this->createForm(PokemonType::class, $pokemon);
        //Soumets le form
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pokemon = $form->getData();

            $manager->persist($pokemon);
            $manager->flush();

            return $this->redirectToRoute('pokemon.all',[
                    'message' =>  $this->addFlash(
                        'success',
                        'Votre pokemon a été mise à jour avec succès !.'
                    )]
            );

        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash(
                'error',
                'Veuillez vérifier les informations dans le formulaire.'
            );
        }

        return $this->render('pokemon/edit.html.twig', [
            'pokemon' => $pokemon->getName(),
            'form' => $form->createView()
        ]);

    }

    /**
     * Permet de supprimer un pokemon
     *
     * @param Pokemon $pokemon
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('delete_pokemon/delete/{id}', name:'poke.delete', methods :['GET','POST'])]
    public function delete(Pokemon $pokemon, Request $request, EntityManagerInterface $manager) : Response{
        // Récupérer l'utilisateur actuellement connecté
        $currentUser = $this->getUser();

        // Vérifier que l'utilisateur possède bien le rôle ROLE_USER
        if (!$this->isGranted('ROLE_USER')) {
            // Redirection vers une page d'accès refusé
            return new RedirectResponse($this->generateUrl('security.connexion'));
        }

        $manager->remove($pokemon);
        $manager->flush();

        $this->addFlash(
            'success',
            'La tâche a bien été supprimé.'
        );

        return $this->redirectToRoute('pokemon.all');
    }
}
