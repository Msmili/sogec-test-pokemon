<?php

namespace App\Security\Voter;

use \App\Entity\Pokemon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use function PHPUnit\Framework\throwException;

class PokemonVoter extends Voter
{
    public const EDIT = 'POKE_EDIT';
    public const DELETE = 'POKE_DELETE';

    private $security;
    private $manager;

    public function __construct(Security $security,EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    /**
     * Permet de vérifier si un utilisateur a le droit d'executer certaines action
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        $supportsAttribute = in_array($attribute, ['POKE_EDIT', 'POKE_DELETE']);
        $supportsSubject = $subject instanceof Pokemon;

        return $supportsAttribute && $supportsSubject;
    }

    /**
     * Permet de gérer les PATCH et DELETE selon les droits utilisateurs
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @param array $context
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token, array $context = []): bool
    {
        $user = $token->getUser();

        // Si l'utisateur n'est pas connecté on autorise pas l'accès
        if (!$user instanceof UserInterface) {
            throw new \InvalidArgumentException("Vous n'ête pas habilité a modifier ou supprimer ce pokemon.");
        }

        switch ($attribute) {
            case self::EDIT:
                    return !$subject->isLegendary();
                break;
            case self::DELETE:
                return !$subject->isLegendary();
                break;
        }

        return false;
    }
}
