<?php

namespace App\Repository;

use App\Entity\Pokemon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * @extends ServiceEntityRepository<Pokemon>
 *
 * @method Pokemon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pokemon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pokemon[]    findAll()
 * @method Pokemon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PokemonRepository extends ServiceEntityRepository
{
    private $security;
    public function __construct(ManagerRegistry $registry, Security $security)
    {
        $this->security = $security;
        parent::__construct($registry, Pokemon::class);
    }

    /**
     * Retourne la liste complète des pokemon sous condition.
     * Connecté on affiche les légendaires sinon sans les légendaires
     * @return array
     */
    public function getAllPokemon() : array{

        $queryBuilder = $this->createQueryBuilder('p');

        if ($this->security->getToken() && $this->security->isGranted('ROLE_USER')) {
            $queryBuilder->where('p.legendary = 1');
        } else {
            $queryBuilder->where('p.legendary = 0');
        }

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();

        return $results;

    }
}
