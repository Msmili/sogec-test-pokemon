<?php
// src/EventListener/PokemonKernelRequestListener.php

    namespace App\EventListener;

    use Symfony\Component\HttpKernel\Event\RequestEvent;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\HttpKernel\KernelEvents;
    use App\Entity\Pokemon;
    use Doctrine\ORM\EntityManagerInterface;

    class PokemonKernelRequestListener implements EventSubscriberInterface
    {
        private $entityManager;

        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
        }
        public static function getSubscribedEvents()
        {
            return [
                KernelEvents::REQUEST => 'onKernelRequest',
            ];
        }

        /**
         * Permet de gérer les données Request avant l'update du pokemon pour déterminer quel data modifié ou non et si ca correspond aux critères
         * @param RequestEvent $event
         */
        public function onKernelRequest(RequestEvent $event)
        {
            $request = $event->getRequest();
            // Check if it's a PATCH request for a Pokemon entity
            if ($request->isMethod('PATCH') && $request->attributes->get('_api_resource_class') === Pokemon::class) {
                $originalPokemon = $request->get('previous_data');
                $pokemon = $this->entityManager->getRepository(Pokemon::class)->find($originalPokemon->getId());
                $requestData = json_decode($request->getContent(), true);

                //On vérifie que le champs type1 avec tout les types pokemon, interdit d'en créer
                if (!in_array(ucfirst($requestData['type1']), array_map('ucfirst', Pokemon::all_types))) {
                    $pokemon->setType1($originalPokemon->getType1());
                } else {
                    $pokemon->setType1(ucfirst($requestData['type1']));
                }


                //On vérifie que le champs type2 avec tout les types pokemon, interdit d'en créer
                if (!in_array(ucfirst($requestData['type2']),  array_map('ucfirst', Pokemon::all_types))) {
                    $pokemon->setType2($originalPokemon->getType2());
                } else {
                    $pokemon->setType2(ucfirst($requestData['type2']));
                }

                //On vérifie qu'on a bien entrer un name sinon on garde l'ancien
                if ($requestData['name'] != 'string') {
                    $pokemon->setName(ucfirst($requestData['name']));
                } else {
                    $pokemon->setName($originalPokemon->getName());
                }

                if ($requestData['generation'] == 0) {
                    $pokemon->setGeneration($originalPokemon->getGeneration());
                } else {
                    $pokemon->setGeneration($requestData['generation']);
                }

                $this->entityManager->flush();
            }
        }
    }
