<?php

    namespace App\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\PasswordType;
    use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Validator\Constraints as Assert;

    class UserPasswordType extends AbstractType {

        /**
         * Création du formulaire pour gérer le password utilisateur
         * @param FormBuilderInterface $builder
         * @param array $options
         */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('password', PasswordType::class, [
                        'label' => 'Mot de passe',
                        'label_attr' => [
                            'class' => 'form_label mt-4'
                        ],
                        'attr' => [
                            'class' => 'form-control',
                            'minlength' => '8',
                            'maxlength' => '255',
                        ],
                    'constraints' => [new Assert\NotBlank()]
                ])
                ->add('submit', SubmitType::class, [
                'attr'=> [
                    'class' => 'btn btn-primary mt-4'
                ],
                'label' => 'Changer le mot de passe.'
            ]);
        }
    }