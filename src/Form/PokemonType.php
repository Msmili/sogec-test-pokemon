<?php

namespace App\Form;

use App\Entity\Pokemon;
use PhpParser\Node\Stmt\Foreach_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PokemonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $pokemonTypes = [];
        $pokemonTypes2 = [];
        //No type ajouter car un pokemon peut ne pas avoir de deuxième type
        $pokemonTypes2['no_type'] = "no_type";

        //Permet d'initialiser un tableau pour afficher les label dans le ChoicesList
        foreach(Pokemon::all_types as $value) {
            $pokemonTypes[$value] = $pokemonTypes2[$value] = $value;
        }

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du pokemon',
                'label_attr' => [
                    'class' => 'form_label mt-4'
                ],
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('type1', ChoiceType::class, [
                'label' => 'Le type du pokemon',
                'label_attr' => [
                    'class' => 'form_label mt-4'
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
                'choices' => $pokemonTypes
            ])
            ->add('type2', ChoiceType::class, [
                'label' => 'Le deuxième type du pokemon',
                'label_attr' => [
                    'class' => 'form_label mt-4'
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
                'choices' => $pokemonTypes2
            ])
            ->add('generation', IntegerType::class, [
                'label' => 'La génération du pokemon',
                'label_attr' => [
                    'class' => 'form_label mt-4'
                ],
                'attr' => [
                    'class' => 'form-control',
                    'min' => 0,
                    'max' => 9
                ]
            ])
            ->add('submit', SubmitType::class, [
                'attr'=> [
                    'class' => 'btn btn-primary mt-4'
                ],
                'label' => 'Modifier mes informations'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pokemon::class,
        ]);
    }
}
