<?php

namespace App\EntityListener;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;

class UserPrePersistListener {

    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }

    /**
     * Permet de gérer les données avant de les insérer en base de données
     *
     * @param PrePersistEventArgs $args
     */
    public function prePersist(PrePersistEventArgs $args){
        $user = $args->getObject();
        $emailValid = $this->isValidEmail($user->getEmail());

        if(!$emailValid) {
            throw new \InvalidArgumentException('Veuillez renseigner un email valide.');
        }

        if(!empty($user->getPassword())) {
            if ($this->isValidPassword($user->getPassword())) {
                $this->encodePassword($user);
            } else {
                throw new \InvalidArgumentException('Le mot de passe ne satisfait pas les critères de sécurité.');
            }
        }else {
            throw new \InvalidArgumentException('Le mot de passe est vide, veuillez le saisir en répondant au critères.');
        }

    }

    public function preUpdate(User $user){
        $this->encodePassword($user);
    }

    /**
     * Encode password based on plain password
     *
     * @param User $user
     * @return void
     */
    public function encodePassword(User $user){
        if ($user->getPlainPassword() === null) {
            return;
        }

        $user->setPassword(
            $this->hasher->hashPassword(
                $user,
                $user->getPlainPassword()
            )
        );
    }

    /**
     * Vérifie que le mot de passe est valide et correspond au critère
     *
     * min : 8 caractères
     * Contient 1 Majuscule, 1 minuscule et 1 caractère minimum
     *
     * @param string $password
     * @return bool
     */
    private function isValidPassword(string  $password) :bool{
        if (strlen($password) < 8) {
            return false;
        }

        if (!preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/', $password)) {
            return false;
        }

        return true;
    }

    /**
     *Vérifie que l'email est valide
     *
     * @param string $email
     * @return bool
     */
    private function isValidEmail(string $email): bool {

        if(empty($email)){
            return false;
        }

        // Utilisez la fonction filter_var pour valider le format de l'e-mail
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

}