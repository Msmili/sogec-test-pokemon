# sogec-test-pokemon

Ce projet a pour but d'évaluer mes compétences en Symfony et Api platform.
L'utiliseur peut se connecter ou s'inscrire et avoir la possibilité de consulté les informations pokemon en base de données.

## Table des Matières

- [Installation](#installation)
- [Import du fichier](#import)
- [Api](#api)
- [Login-Check](#login-check)
- [User](#user)
- [Pokemon](#pokemon)
## Installation

```
cd existing_repo
git remote add origin https://gitlab.com/Msmili/sogec-test-pokemon.git
git branch -M main
git push -uf origin main
```

## Import

Pour importer le fichier pokemon.csv il suffit de faire la commande suivante : 
```
php bin/console app:import:csv chemin du fichier
```

## Integrate with your tools

## Api
### Login Check
/api/login -> Connexion de l'utilisateur
### User
[POST] /api/users -> Inscription de l'utiliseur<br>
[GET] /api/users/{id} -> Récupération des informations utilisateur<br>
[PUT] /api/users/{id} -> Modification des informations utilisateur
### Pokemon
[GET] /api/pokemon -> Récupération de la collection pokemon <br>
[GET] /api/pokemon/{id} -> Récupération des informations d'un pokemon<br>
[DELETE] /api/pokemon/{id} -> Supprime un pokemon <br>
[PATCH] /api/pokemon/{id} -> Modifie les informations d'un pokemon <br>


